package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EditContatoPage extends BasePage2 {

	public EditContatoPage(WebDriver driver, int contato_id) {
		super(driver, "editar/" + contato_id);
	}

	public ContatoPage alteraDadosContato(int contato_id) {
		WebElement telefone = driver.findElement(By.name("telefone"));
		WebElement email = driver.findElement(By.name("email"));
		
		telefone.clear();
		email.clear();
		telefone.sendKeys("+55(11)98348-0123");
		email.sendKeys("esseemailfoialterado@gmail.com");
		driver.findElement(By.className("btnupload-form")).click();
		
		return new ContatoPage(driver, contato_id);
	}

}
