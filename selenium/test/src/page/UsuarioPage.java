package page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UsuarioPage extends BasePage{

	public UsuarioPage(WebDriver driver) {
		super(driver, "");
	}

	public ContatoPage selecionaUsuario(int i) {
		List<WebElement> link = driver.findElements(By.cssSelector("tr a"));
		link.get(0).click();
		return new ContatoPage(driver, i);
	}

}
