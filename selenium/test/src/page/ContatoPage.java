package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContatoPage extends BasePage2 {

	public ContatoPage(WebDriver driver, int usuario_id) {
		super(driver, "Lista_contato/criar/" + usuario_id);
	}

	public boolean temNomeUsuario(String usuario) {
		return driver.getPageSource().contains(usuario);
	}

	public boolean temContatos() {
		return driver.getPageSource().contains("N�mero do Telefone");
	}
	
	public boolean formularioOculto() {
		WebElement element = driver.findElement(By.id("novo_contato"));
		return ! hasClass(element, "show");
	}
	
	public boolean formularioExibido() {
		WebElement button = driver.findElement(By.id("collapsebutton"));
		button.click();
		
		ExpectedCondition<Boolean> condition = ExpectedConditions.attributeContains(By.id("novo_contato"), "class", "show");
		return new WebDriverWait(driver, 10).until(condition);
	}
	
	public boolean criaContatos() {
		if(formularioExibido()) {
			WebElement nome = driver.findElement(By.name("nome"));
			WebElement telefone = driver.findElement(By.name("telefone"));
			WebElement email = driver.findElement(By.name("email"));
			
			nome.sendKeys("Lucas");
			telefone.sendKeys("+55(11)98348-0122");
			email.sendKeys("emaildolucasp@gmail.com");
			
			WebElement submit = driver.findElement(By.className("btnupload-form"));
			submit.click();
			
			return driver.getPageSource().contains("+55(11)98348-0122") && 
			driver.getPageSource().contains("emaildolucasp@gmail.com");
		}
		return false;
	}

	public EditContatoPage editaContatos(int contato_id) {
		criaContatos();
		driver.findElement(By.id("editar_" + contato_id)).click();
		return new EditContatoPage(driver, contato_id);
	}

	public boolean comfirmaEdicao() {
		return driver.getPageSource().contains("+55(11)98348-0123") && 
		driver.getPageSource().contains("esseemailfoialterado@gmail.com");
	}

	public DeletarContatoPage removeContato(int contato_id) {
		criaContatos();
		driver.findElement(By.id("deletar_" + contato_id)).click();
		return new DeletarContatoPage(driver, contato_id);
	}

	public boolean comfirmaRemocao() {
		return ! temContatos();
	}
}
