package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DeletarContatoPage extends BasePage2 {

		public DeletarContatoPage(WebDriver driver, int contato_id) {
			super(driver, "deletar/" + contato_id);
		}
		
		public ContatoPage removeContato(int contato_id) {
			driver.findElement(By.className("delete-btn")).click(); 
			return new ContatoPage(driver, contato_id);
		}


}
