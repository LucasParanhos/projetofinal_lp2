package page;

import org.openqa.selenium.WebDriver;

public class E2E2Page extends BasePage2 {

	public E2E2Page(WebDriver driver) {
		super(driver, "test/E2ETest");
	}

	public boolean limpaTabelaDeTeste() {
		return driver.getPageSource().contains("PASSOU");
	}

}
