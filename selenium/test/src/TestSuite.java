

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.ContatoTest;
import test.TarefaTest;
import test.TurmaTest;
import test.UsuarioTest;

@RunWith(Suite.class)
@SuiteClasses({
	TarefaTest.class,
	TurmaTest.class,
	UsuarioTest.class,
	ContatoTest.class
})
public class TestSuite {}
