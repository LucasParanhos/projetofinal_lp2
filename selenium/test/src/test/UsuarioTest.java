package test;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import page.ContatoPage;
import page.UsuarioPage;

public class UsuarioTest extends BaseTest {
	@Test
	public void selecionaUsuario() {
		
		UsuarioPage UsuarioPage = new UsuarioPage(driver);
		ContatoPage ContatoPage = UsuarioPage.selecionaUsuario(1);
		boolean b = ContatoPage.temNomeUsuario("Jo�o de Souza");
		assertTrue("Erro na sele��o do usu�rio", b);
		
		b = ! ContatoPage.temContatos();
		assertTrue("N�o deveria haver contatos na p�gina", b);
		
		b = ContatoPage.formularioOculto();
		assertTrue("O formul�rio de cadastro das contatos deveria estar oculto", b);

		b = ContatoPage.formularioExibido();
		assertTrue("O formul�rio de cadastro das contatos deveria estar na tela", b);
		
	}
}


