package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import page.EditContatoPage;
import page.ContatoPage;
import page.DeletarContatoPage;

public class ContatoTest extends BaseTest {

	@Test
	public void criaContato() {
		ContatoPage ContatoPage = new ContatoPage(driver, 2);
		boolean b = ContatoPage.criaContatos();
		assertTrue("Os contatos n�o foram criadas", b);
	}
	
	@Test
	public void editaContato() {
		int usuario_id = 2;
		int contato_id = 1;
		
		ContatoPage ContatoPage = new ContatoPage(driver, usuario_id);
		EditContatoPage editPage = ContatoPage.editaContatos(contato_id);
		ContatoPage = editPage.alteraDadosContato(usuario_id);
		boolean b = ContatoPage.comfirmaEdicao();
		assertTrue("Erro na edi��o de contatos", b);	
	}
	
	@Test
	public void removeContato() {
		int usuario_id = 3;
		int contato_id = 1;
		
		ContatoPage ContatoPage = new ContatoPage(driver, usuario_id);
		DeletarContatoPage deletePage = ContatoPage.removeContato(contato_id);
		ContatoPage = deletePage.removeContato(contato_id);
		boolean b = ContatoPage.comfirmaRemocao();
		assertTrue("Erro na remo��o de contatos", b);	
	}
	
}
