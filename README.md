# Módulo de lista de contatos

### Abrindo o sistema
basta acessar localhost/modulo/lista_contato

### Utilizando o sistema

##### seleção do usuário
   Nesse módulo nós podemos criar e editar os contatos de um Usuário (Módulo ilustrativo para que possua total controle, seus dados normalmente estariam cadastrados no banco), você deverá selecionar o usuário em [http://localhost/modulo/lista_contato/](http://localhost/modulo/lista_contato/ "http://localhost/modulo/lista_contato/"), para entrar na lista de contatos basta clicar na linha em cima do nome.

##### Adição e edição de contatos
Após abrir este usuário poderá ver uma tabela com os contatos já cadastrados e um botão para adicionar um novo, clique em novo contato e adicione quantos quiser, os métodos de edição e exclusão funcionam normalmente.

###Padrões de projeto utilizados
- Factory
- Model-View-Controller (MVC)
- Hierarchical Model View Controller (HMVC)

###Testes
os testes estão disponiveis nos seguintes urls
##### Testes unitarios
1. [Teste ilustrativo do usuário](http://localhost/modulo/lista_contato/test/UsuarioTest "teste ilustrativo do usuário")
2. [Teste do módulo lista_contato](http://localhost/modulo/lista_contato/test/ContatoUsuarioTest "Teste do módulo lista_contato")

##### Teste de regreção
Esse teste está disponivel em [http://localhost/modulo/lista_contato/test/all](http://localhost/modulo/lista_contato/test/all "http://localhost/modulo/lista_contato/test/all")

##### Teste automatizado
O processo de instalação é igual ao passado pelo professor (disponivel [aqui](https://bitbucket.org/profpradoifsp/at03-hmvc/src/master/README.md "aqui"))

Todos os testes já estão configurados para se iniciar junto com  a execução do controlador RegressionTest.java