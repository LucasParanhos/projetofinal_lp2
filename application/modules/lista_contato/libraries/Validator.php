<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Validator extends CI_Object{

    public function form_contato(){
        $this->form_validation->set_rules('nome', 'nome', 'trim|required|min_length[3]|max_length[128]');
        $this->form_validation->set_rules('telefone', 'telefone', 'trim|required|min_length[9]|max_length[17]|regex_match[^\+[0-9]{2}\([0-9]{2}\)[0-9]{5}\-[0-9]{4}$^]');
        $this->form_validation->set_rules('email', 'email', 'trim|required|min_length[3]|max_length[128]');
        return $this->form_validation->run();
    }

}