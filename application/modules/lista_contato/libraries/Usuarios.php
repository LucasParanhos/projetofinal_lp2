<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/Dao.php';

class Usuarios extends Dao{

    function __construct(){
        parent::__construct('usuarios');
    }
    
    /**
     * Lista estática de usuarios. Usada apenas para simular a geração 
     * de conteúdo no módulo responsável pelo registro dos contatos.
     * Este conteúdo, naturalmente, deveria vir do bd.
     */
    public function lista(){
        return array(
            array('id' => 1, 'João de Souza', '11993026031', '01/01/2001'),
            array('id' => 2, 'Maria de Souza', '11993819345', '06/03/1967'),
            array('id' => 3, 'Lucas Paranhos', '11930185938', '10/09/2000'),
            array('id' => 4, 'Pedro Felipe', '11910499604', '20/05/1997'),
        );
    }
        
    /**
     * Determina o nome de um usuario.
     * @param int usuario_id
     * @return string nome do usuario
     */
    public function nome($usuario_id){
        $v = array('', 'João de Souza', 'Maria de Souza', 'Lucas Paranhos', 'Pedro Felipe');
        return $v[$usuario_id];
    }
}