<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ContatoUsuario extends Dao {

    function __construct(){
        parent::__construct('contato_usuario');
    }

    public function insert($data, $table = null) {
        // mais uma camada de segurança... além da validação
        $cols = array('nome','telefone' , 'email', 'usuario_id');
        $this->expected_cols($cols);

        return parent::insert($data);
    }
}