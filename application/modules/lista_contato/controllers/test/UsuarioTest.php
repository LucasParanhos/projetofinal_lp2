<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/lista_contato/libraries/Usuarios.php';

/**
 * Essa classe de teste é apenas ilustrativa e foi criada somente 
 * para que você perceba a utilidade da execução de uma suite de testes.
 */

class UsuarioTest extends Toast{
    private $usuario;

    function __construct(){
        parent::__construct('UsuarioTest');
    }

    function _pre(){
        $this->usuario = new Usuarios();
    }

    function test_carrega_lista_de_usuarios(){
        $v = $this->usuario->lista();
        $this->_assert_equals(4, sizeof($v), "Número de usuários está incorreto");
    }

    function test_gera_nome_dos_usuarios(){
        $nome = $this->usuario->nome(2);
        $this->_assert_equals('Maria de Souza', $nome, "Erro no nome do usuário");
    }

}