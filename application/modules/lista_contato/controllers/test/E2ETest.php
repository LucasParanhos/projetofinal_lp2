<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/lista_contato/libraries/ContatoUsuario.php';
include_once APPPATH . 'modules/lista_contato/controllers/test/builder/ContatoUsuarioDataBuilder.php';

class E2ETest extends Toast{

    function __construct(){
        parent::__construct('E2E Test');
    }

    function test_limpa_tabela_de_teste(){
        $builder = new ContatoUsuarioDataBuilder('lp2_lista_contato');
        $builder->clean_table();

        $contato = new ContatoUsuario();
        $data = $contato->get();
        $this->_assert_equals_strict(0, sizeof($data), 'Erro na limpeza da tabela de teste');
    }

}