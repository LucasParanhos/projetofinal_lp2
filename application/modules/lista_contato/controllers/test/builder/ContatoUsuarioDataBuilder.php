<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class ContatoUsuarioDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_lista_contato_test'){
        parent::__construct('contato_usuario', $table);
    }

    function getData($index = -1){
        $data[0]['nome'] = 'Teste 1';
        $data[0]['telefone'] = '+55(11)91234-5678';
        $data[0]['email'] = 'teste1@gmail.com';
        $data[0]['usuario_id'] = 5;

        $data[1]['nome'] = 'Teste 2';
        $data[1]['telefone'] = '+55(11)14123-2212';
        $data[1]['email'] = 'teste2@gmail.com';
        $data[1]['usuario_id'] = 6;

        $data[2]['nome'] = 'Teste 3';
        $data[2]['telefone'] = '+55(11)23123-8564';
        $data[2]['email'] = 'teste3@gmail.com';
        $data[2]['usuario_id'] = 7;
        return $index > -1 ? $data[$index] : $data;
    }

}