<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/lista_contato/libraries/ContatoUsuario.php';
include_once APPPATH . 'modules/lista_contato/controllers/test/builder/ContatoUsuarioDataBuilder.php';

class ContatoUsuarioTest extends Toast{
    private $builder;
    private $contato;

    function __construct(){
        parent::__construct('ContatoUsuarioTest');
    }

    function _pre(){
        $this->builder = new ContatoUsuarioDataBuilder();
        $this->contato = new ContatoUsuario();
    }

    // apenas ilustrativo
    function test_objetos_criados_corretamente(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->contato, "Erro na criação do contato");
    }

    // apenas ilustrativo
    function test_selecionado_banco_de_teste(){
        $s = $this->builder->database();
        $this->_assert_equals('lp2_lista_contato_test', $s, 'Erro na seleção do banco de teste');
    }
    
    // function valida_telefone_corretamente(){

    // }

    
    function test_insere_registro_na_tabela(){
        // cenário 1: vetor com dados corretos
        $this->builder->clean_table();
        $data = $this->builder->getData(0);
        $id1 = $this->contato->insert($data);
        $this->_assert_equals(1, $id1, "Esperado 1, recebido $id1");

        // verificação: o objeto criado é, de fato, aquele que enviamos?
        $task = $this->contato->get(array('id' => 1))[0];
        $this->_assert_equals($data['nome'], $task['nome']);
        $this->_assert_equals($data['telefone'], $task['telefone']);
        $this->_assert_equals($data['email'], $task['email']);
        $this->_assert_equals($data['usuario_id'], $task['usuario_id']);

        // cenário 2: vetor vazio
        $id2 = $this->contato->insert(array());
        $this->_assert_equals(-1, $id2, "Esperado -1, recebido $id2");

        // cenário 3: vetor com dados inesperados
        $info = $this->builder->getData(1);
        $info['unexpected_col_name'] = 1;
        $id = $this->contato->insert($info);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");

        // cenário 4: vetor com dados incompletos... deve ser
        // tratado pela validação, mas tem que ser pensado aqui
        $v = array('nome' => 'vetor incompleto');
        $id = $this->contato->insert($v);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");
    }

    function test_carrega_todos_os_registros_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $tasks = $this->contato->get();
        $this->_assert_equals(3, sizeof($tasks), "Número de registros incorreto");
    }

     function test_carrega_registro_condicionalmente(){
         $this->builder->clean_table();
         $this->builder->build();     
         
         $task = $this->contato->get(array('nome' => 'Teste 1', 'usuario_id' => 5))[0];
         $this->_assert_equals('Teste 1', $task['nome'], "Erro no nome");
         $this->_assert_equals(5, $task['usuario_id'], "Erro no id do usuario");
     }
    
    function test_atualiza_registro(){
        $this->builder->clean_table();
        $this->builder->build();

        // lê um registro do bd
        $task1 = $this->contato->get(array('id' => 1))[0];
        $this->_assert_equals('Teste 1', $task1['nome'], "Erro no nome");
        $this->_assert_equals(5, $task1['usuario_id'], "Erro no id do usuario");

        // atualiza seus valores
        $task1['nome'] = 'Teste 01';
        $task1['usuario_id'] = 8;
        $this->contato->insert_or_update($task1);

        // lê novamente, o mesmo objeto, e verifica se foi atualizado
        $task2 = $this->contato->get(array('id' => 1))[0];
        $this->_assert_equals('Teste 01', $task2['nome'], "Erro no nome");
        $this->_assert_equals(8, $task2['usuario_id'], "Erro no id do usuario");
    }
    
    function test_remove_registro_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        // verifica que o registro existe
        $task1 = $this->contato->get(array('id' => 1))[0];
        $this->_assert_equals('Teste 1', $task1['nome'], "Erro no nome");
        $this->_assert_equals(5, $task1['usuario_id'], "Erro no id do usuario");

        // remove o registro
        $this->contato->delete(array('id' => 1));

        // verifica que o registro não existe mais
        $task2 = $this->contato->get(array('id' => 1));
        $this->_assert_equals_strict(0, sizeof($task2), "Registro não foi removido");
    }

}