<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Lista_contato extends MY_Controller{

    public function __construct(){
        $this->load->model('Lista_contatoModel', 'model');
    }

    /**
     * Página que exibe a lista de usuarios. Note que listar as usuarios não 
     * é responsabilidade deste módulo, mas é importante para o seu funcionamento.
     * Esta página foi criada para que você possa entender a dependência entre 
     * módulos e como você deve lidar com esta situação.
     * 
     * O módulo "lista_contato" é dependente do módulo "usuarios". Por este motivo, simulamos 
     * o comportamento do módulo "turma" usando conteúdo estático que serve apenas 
     * para que o uso do módulo "tarefa" seja o mais realista possível.
     */
    public function index(){
        $data['titulo'] = 'Lista de usuarios';
        $data['rotulo_botao'] = 'Novo usuario';
        $data['form_subject'] = 'novo_usuario';
        $data['show_form'] = false;
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['formulario'] = $this->load->view('fake_form', $data, true);
        $data['lista'] = $this->model->fake_list();

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }

    /**
     * Página para criação de contatos
     * @param int usuario_id: o id do usuario para a qual será direcionada o contato
     */
    public function criar($usuario_id){
        $this->add_script('contato/mascara');
        
        $this->validate_id($usuario_id);
        $usuario = $this->model->nome_usuario($usuario_id);
        $data['show_form'] = $this->model->novo_contato($usuario_id);

        $data['home'] = true;
        $data['titulo'] = "Contatos do Usuario - $usuario";
        $data['rotulo_botao'] = 'Novo contato';
        $data['form_subject'] = 'novo_contato';
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['formulario'] = $this->load->view('form_contato', $data, true);
        $data['lista'] = $this->model->lista_contatos($usuario_id);

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }

    /**
     * Página para edição dos contatos
     * @param int contato_id: o id do contato editado
     */
    public function editar($contato_id){
        $this->validate_id($contato_id);
        $usuario_id = $this->model->edita_contato($contato_id);
        $usuario = $this->model->nome_usuario($usuario_id);
        $data['show_form'] = true;

        $data['titulo'] = "Editar contato do usuário - $usuario";
        $data['rotulo_botao'] = 'Novo Contato';
        $data['form_subject'] = 'novo_contato';
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['formulario'] = $this->load->view('lista_contato/form_contato', $data, true);

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }

    public function deletar($contato_id){
        $this->validate_id($contato_id);
        $data = $this->model->deleta_contato($contato_id);
        $turma = $this->model->nome_usuario($data['usuario_id']);

        $data['home'] = true;
        $data['titulo'] = "Remover contato do usuario - $turma";
        $data['usuario'] = $this->model->nome_usuario($data['usuario_id']);
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $html = $this->load->view('confirm_delete', $data, true);
        $this->show($html);
    }

}