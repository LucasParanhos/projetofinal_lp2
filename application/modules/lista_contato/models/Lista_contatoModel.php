<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/component/buttons/EditDeleteButtonGroup.php';
include_once APPPATH.'libraries/component/Table.php';

class Lista_contatoModel extends CI_Model{

    /**
     * Gera a tabela que contém a lista de usuarios
     */
    public function fake_list(){
        $this->load->library('Usuarios');
        $data = $this->usuarios->lista();
        $header = array('#', 'Nome', 'Telefone', 'Aniversário');
        $table = new Table($data, $header);
        $table->action('Lista_contato/criar');
        $table->zebra_table();
        $table->use_border();
        $table->use_hover();
        return $table->getHTML();
    }

    /**
     * Gera a lista de contatos de uma usuario cadastradas no bd
     * @param int usuario_id: o identificador do usuario 
     * @return string: código html da tabela
     */
    public function lista_contatos($usuario_id){
        $header = array('#', 'Nome', 'Número do Telefone', 'Email');
        $this->load->library('ContatoUsuario', null, 'contato');
        $this->contato->cols(array('id', 'nome', 'telefone', 'email'));
        $data = $this->contato->get(array('usuario_id' => $usuario_id));
        if(! sizeof($data)) return '';
        
        $table = new Table($data, $header);
        $table->zebra_table();
        $table->use_border();
        $table->use_hover();
        
        $edbg = new EditDeleteButtonGroup('lista_contato');
        $table->use_action_button($edbg);
        return $table->getHTML();
    }

    /**
     * Registra um contato no bd
     * @param $_POST['Nome', 'Telefone', 'Email']
     * @return boolean true caso ocorra erro de validação
     */
    public function novo_contato($usuario_id){
        if(! sizeof($_POST)) return;
        $this->load->library('Validator', null, 'valida');

        if($this->valida->form_contato()){
            $this->load->library('ContatoUsuario', null, 'contato');
            $data = $this->input->post();
            $data['usuario_id'] = $usuario_id;
            $this->contato->insert($data);
        }
        else return true;
    }

    /**
     * Atualiza os dados de um contato
     * @param int contato_id: o identificador do contato
     * @param int id: o identificador do usuario | redireciona para página principal
     */
    public function edita_contato($contato_id){
        $this->load->library('ContatoUsuario', null, 'contato');
        $this->load->library('Validator', null, 'valida');
        $task = $this->contato->get(array('id' => $contato_id));

        if(sizeof($_POST) && $this->valida->form_contato()){
            $data = $this->input->post();
            $data['id'] = $contato_id;
            $id = $this->contato->insert_or_update($data);
            if($id) redirect('lista_contato/criar/'.$task[0]['usuario_id']);
        }
        else {
            foreach ($task[0] as $key => $value)
                $_POST[$key] = $value;
            return $_POST['usuario_id'];
        }
    }

    /**
     * Elimina um contato do bd.
     * @param int contato_id: o identificador do contato
     * @param array: os dados do contato | redireciona para página principal
     */
    public function deleta_contato($contato_id) {
        $this->load->library('ContatoUsuario', null, 'usuario');
        $task = $this->usuario->get(array('id' => $contato_id));
        
        if(sizeof($_POST)) {
            if($this->usuario->delete(array('id' => $contato_id)))
                redirect('lista_contato/criar/'.$task[0]['usuario_id']);
        }
        else return $task[0];
    }

  /**
     * Determina o nome de um usuario.
     * @param int usuario_id
     * @return string nome do usuario
     */
    public function nome_usuario($usuario_id){
        $this->load->library('Usuarios');
        return $this->usuarios->nome($usuario_id);
    }
}