<div class="container mt-3 <?= $show_form ? '' : 'collapse' ?>" id="novo_usuario">
    <div class="card">
        <div class="card-header"><h4>Dados do usuario</h4></div>
        <div class="card-body">
            <form method="POST" class="text-center border border-light p-4">
                <div class="form-row mb-4">
                    <div class="col-md-6">
                        <input type="text" name="nome" value="<?= set_value('nome') ?>" class="form-control" placeholder="Nome">
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="Telefone" value="<?= set_value('Telefone') ?>" class="form-control" placeholder="Telefone" maxlength="11">
                    </div>
                </div>

                <div class="form-row mb-4">
                    
                <div class="col-md-12">
                        <input type="date" name="dataNascimento" value="<?= set_value('dataNascimento') ?>" class="form-control" placeholder="Data de nascimento (aaaa-mm-dd)" maxlength="8">
                    </div>
                </div>
                <div class="text-center text-md-right">
                    <a class="btnupload-form btn btn-primary">Enviar</a>
                </div>
            </form>
        </div>
    </div>
</div>