<div class="container mt-3 <?= $show_form ? '' : 'collapse' ?>" id="novo_contato">
    <div class="card">
        <div class="card-header"><h4>Contato</h4></div>
        <div class="card-body">
            <form method="POST" class="text-center border border-light p-4" id="task-form">
                <div class="form-row mb-4">
                    <div class="col-md-6">
                        <input type="text" name="nome" value="<?= set_value('nome') ?>" class="form-control" placeholder="Nome" maxlength="128">
                    </div>
                    <div class="col-md-6">
                        <input type="text" id="telefone" name="telefone" value="<?= set_value('telefone') ?>" class="form-control" placeholder="Telefone" maxlength="17">
                    </div>
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-12">
                        <input type="text" name="email" value="<?= set_value('email') ?>" class="form-control" placeholder="Email" maxlength="128">
                    </div>           
                </div>

                <div class="text-center text-md-right">
                    <a class="btnupload-form btn btn-primary" onclick="document.getElementById('task-form').submit();">Enviar</a>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js" type="text/javascript"></script>

<script>
$("#telefone").mask("+99(99)99999-9999");
</script>