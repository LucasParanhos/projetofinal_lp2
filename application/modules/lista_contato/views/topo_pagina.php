<div class="row">
        <div class="col-md-6">
            <a href="<?= base_url('Lista_contato') ?>" class="btn btn-primary  <?= isset($home) ? '' : 'd-none' ?>"><i class="fas fa-home"></i></a>
        </div>
        <div class="col-md-6 text-right">
            <p>Version 2.0.0</p>
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <h2><?= $titulo ?></h2>
    </div>